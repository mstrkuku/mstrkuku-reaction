var React = require('react');
var ReactDOM = require('react-dom');
require('./index.sass');
require('./assets/styles/global.css');
const constants = require('./utils/constants.js');
var App = require('./components/App');


ReactDOM.render(
    <App />,
    document.getElementById('app')
);
