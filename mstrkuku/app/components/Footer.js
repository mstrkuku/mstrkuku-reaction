var React = require('react');
var Link = require('react-router-dom').Link;

const constants = require('../utils/constants');


class Footer extends React.Component {
    render() {
        return (
            <footer>
                <ul className="social-icons">
                    <li className="fb-icon">
                        <a target="_blank" href={constants.FB_URL}>
                            <img src="./app/assets/icons/social/facebook.svg" alt="fb-icon" title="fb" name="fb-icon"/>
                        </a>
                    </li>

                    <li className="ig-icon">
                        <a target="_blank" href={constants.IG_URL}>
                            <img src="./app/assets/icons/social/instagram.svg" alt="ig-icon" title="ig" name="ig-icon"/>
                        </a>
                    </li>

                    <div className='foot-note'>
                        <span>
                            <b><span className='mr-orange'>mstr</span>kuku </b>© 2017
                        </span>
                    </div>

                    <div>
                        <Link className='credits' to='/credit'>Credits</Link>
                    </div>
                </ul>
                {/*<ul></ul>*/}

            </footer>
        )
    }
}

module.exports = Footer;