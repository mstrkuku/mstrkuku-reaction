var React = require('react');
const constants = require('../utils/constants');
// var Link = require('react-router-dom').Link;
var NavLink = require('react-router-dom').NavLink;


class Nav extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isToggleOn: true
        };

        // this.handleNavToggleClick = this.handleNavToggleClick(this);
        this.handleNavToggleOn = this.handleNavToggleOn.bind(this);
        this.handleNavToggleOff = this.handleNavToggleOff.bind(this);

    }

    // handleNavToggleClick() {
    //     this.setState(prevState => ({
    //         isToggleOn: !prevState.isToggleOn,
    //     }));
    // }

    handleNavToggleOn() {
        this.setState(prevState => ({
            isToggleOn: true,
        }));
    }

    handleNavToggleOff() {
        this.setState(prevState => ({
            isToggleOn: false,
        }));
    }

    render() {
        var Navigation = [] = constants.NAV_ARRAY;
        // console.log(NavLink);

        var activeClass = document.getElementsByClassName('active');
        // console.log(activeClass);


        var x = activeClass["0"];

        var b = x;
        // console.info(b);


        return (
            <div className='header'>

                <section className='mk-icon' alt='mrkuku icon'>
                    <NavLink exact activeClassName='active' to='/'>
                        <img src={constants.MR_KUKU_RUN_IMG} alt='Welcome' title='Welcome'/>
                    </NavLink>
                </section>

                <section onMouseEnter={this.handleNavToggleOn} onMouseLeave={this.handleNavToggleOff}
                         className={this.state.isToggleOn ? 'nav-container' : 'nav-container shorter-height'}>
                    <ul>
                        <li className='nav-toggle btn-container-2'>
                            <button className='nav-toggle-btn'>
                                <span className='pulse-button-2'></span>
                            </button>
                        </li>
                    </ul>

                    <ul className={this.state.isToggleOn ? 'nav toggleOn' : 'nav toggleOff'}>
                        {Navigation.map(function (navItem) {
                            return (
                                <li key={navItem}>
                                    <NavLink exact activeClassName='active' to={'/' + navItem} title={navItem}>
                                        {navItem}
                                    </NavLink>
                                </li>
                            )
                        })}
                    </ul>

                    <div className={this.state.isToggleOn ? 'toggleOff' : 'toggleOn'}>
                        {Navigation.map(function (navItem) {
                            return (
                                <NavLink key={navItem} exact activeClassName='active-nav-title' to={'/' + navItem} title={navItem}>
                                    <h1 className={'nav-title ' + navItem}>{navItem}</h1>
                                </NavLink>
                            )
                        })}
                    </div>

                </section>

                <section className='mk-tag' alt='mrkuku tag'>
                    <img src={constants.MR_KUKU_TAG_IMG}/>
                </section>


            </div>
        );
        // console.info(x);
    }
}

module.exports = Nav;
