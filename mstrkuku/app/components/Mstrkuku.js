var React = require('react');
var Link = require('react-router-dom').Link;
const constants = require('../utils/constants');

class mstrkuku extends React.Component {
    render() {
        return(
            <div className='about-container'>

                {/*<h1 className='headline'>100% Gluten Free Designs!</h1>*/}

                <div className='about-mstrkuku'>

                    <section className='blurb left'>

                        <span className='big-body'>I go by Mr. Kuku, your friendly neighborhood graphic designing,
                            illustrating, creative developer. My whole "thing" is to make things look
                            cool and display a certain level of effective & efficient, <b>100% gluten free</b>, quality to any brand.
                        </span>

                        <span className='sub-body'>I am easy to work with. So if you do need anything,
                            just reach OUT, I'll contact you ASAP.</span>

                    </section>

                    <section className='lets-work right'>

                        <div>
                            <img src={constants.THEKUS} />
                        </div>

                        <div>
                            <Link className='primary-btn btn' to='/request' disabled='true'>Let's Work!</Link>
                        </div>

                    </section>

                </div>

            </div>
        )
    }
}

module.exports = mstrkuku;