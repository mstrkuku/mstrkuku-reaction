// DEPENDENCIES

var React = require('react');
var ReactRouter = require('react-router-dom');
var Router = ReactRouter.BrowserRouter;
var Route = ReactRouter.Route;
var Switch = ReactRouter.Switch;

//COMPONENTS
var Nav = require('./Nav');
var Splash = require('./Splash');
var Mstrkuku = require('./Mstrkuku');
var Gallery = require('./Gallery');
var Blog = require('./Blog');
var Request = require('./Request');
// var Question = require('./Question');
var Footer = require('./Footer');
var Credit = require('./Credit');

var api = require('../utils/api');

//TEMPORARY COMPONENTS
var ComingSoon = require('./MrKukuComingSoon');
const constants = require('../utils/constants');



class App extends React.Component {
    // componentDidMount() {
    //     api.getDeviantAccess();
    // }

    componentWillUnmount() {
        sessionStorage.setItem('DeviantAccessed', 'true');
    }
    render() {
        return (
                <Router>
                    <div className='container'>
                        <Nav/>
                        <Switch>
                            <Route exact path='/' component={Splash}/>
                            <Route path='/mstrkuku' component={Mstrkuku}/>
                            <Route path='/gallery' component={Gallery} />
                            <Route path='/blog' component={Blog}/>
                            <Route path='/request' component={Request}/>
                            {/*<Route path='/store' render={function(){return <h1 className='flex-centered'>COMING SOON BRUH!</h1>}}/>*/}
                            <Route path='/store' component={ComingSoon}/>
                            <Route exact path='/credit' component={Credit}/>
                            {/*<Route path='/question' component={Question}/>*/}
                            <Route render={function() {return <p>Not> Found</p>}}/>
                        </Switch>
                        <Footer/>
                    </div>
                </Router>
        )
    }
}

module.exports = App;