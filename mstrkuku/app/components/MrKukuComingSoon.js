var React = require('react');
const constants = require('../utils/constants');

class ComingSoon extends React.Component {
    render() {
        return (
            <div className='coming-soon flex-centered'>
                <video className='coming-soon-video' autoPlay>
                    <source src={constants.MR_KUKU_COMING_SOON}/>
                </video>
            </div>
        )
    }
}

module.exports = ComingSoon;