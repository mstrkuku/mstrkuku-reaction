var React = require('react');
const constants = require('../utils/constants');
var NavLink = require('react-router-dom').NavLink;
// var Footer = require('./Footer');


class Splash extends React.Component {
    render() {

        var backgroundImageStyle = {
            backgroundImage: "url(" + './app/assets/img/ArtFam-opaque.png' + ")",
            backgroundSize: "cover",
        };

        var Navigation = [] = constants.NAV_ARRAY;

        return (
            <div className='splash'>
                <div className='splash-container' style={backgroundImageStyle}>

                    <section>
                        <div className='mk-icon-splash'><img src={constants.MR_KUKU_RUN_IMG}/></div>

                        <div className='splash-nav-container'>
                            <ul className='splash-nav'>
                                {Navigation.map(function(navItem){
                                    return (
                                        <li key={navItem}>
                                            <NavLink exact activeClassName='splash-active' to={'/' + navItem} title={navItem}>
                                                {navItem}
                                            </NavLink>
                                            <span className='left-arrow'><img src={constants.LEFT_ARROW}/></span>
                                        </li>
                                    )
                                })}
                            </ul>
                        </div>

                    </section>


                    <section>
                        <div className='mk-tag-splash'>
                            <img src={constants.MR_KUKU_TAG_IMG}/>
                        </div>

                        <div className='social-icons'>
                            <ul className="social-icons">

                                <li className="fb-icon">
                                    <a target="_blank" href={constants.FB_URL}>
                                        <img src="./app/assets/icons/social/facebook.svg" alt="fb-icon" title="fb" name="fb-icon"/>
                                    </a>
                                </li>

                                <li className="ig-icon">
                                    <a target="_blank" href={constants.IG_URL}>
                                        <img src="./app/assets/icons/social/instagram.svg" alt="ig-icon" title="ig" name="ig-icon"/>
                                    </a>
                                </li>

                                <span className='mk-foot'>
                                    <b><span className='mr-orange'>mstr</span>kuku </b>© 2017
                                </span>
                            </ul>

                        </div>

                    </section>
                </div>
            </div>
        )
    }
}

module.exports = Splash;