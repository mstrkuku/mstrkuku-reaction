var React = require('react');
const constants = require('../utils/constants');

class Blog extends React.Component {
    render() {
        return (
            <div className='blog-container'>
                <ul className='blog-options'>
                    <li className='blog-item'>
                        <div className='blog-item-container'>
                            <span className='date'>Jan 1 - 12pm - Kuku</span>
                            <span className='banner front'><h2>Blog Post</h2></span>
                            <span><img src={constants.MR_KUKU_ORIG_IMG} alt='blog-img'/></span>
                            <span className='banner behind'><h2>&nbsp;</h2></span>
                        </div>
                    </li>

                    <li className='blog-item'>
                        <div className='blog-item-container'>
                            <span className='date'>Jan 1 - 12pm - Kuku</span>
                            <span className='banner front'><h2>Blog Post</h2></span>
                            <span><img src={constants.MR_KUKU_ORIG_IMG} alt='blog-img'/></span>
                            <span className='banner behind'><h2>&nbsp;</h2></span>
                        </div>
                    </li>

                    <li className='blog-item'>
                        <div className='blog-item-container'>
                            <span className='date'>Jan 1 - 12pm - Kuku</span>
                            <span className='banner front'><h2>Blog Post</h2></span>
                            <span><img src={constants.MR_KUKU_ORIG_IMG} alt='blog-img'/></span>
                            <span className='banner behind'><h2>&nbsp;</h2></span>
                        </div>
                    </li>
                </ul>
            </div>
        )
    }
}

module.exports = Blog;