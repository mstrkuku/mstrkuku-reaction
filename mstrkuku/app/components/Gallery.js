var React = require('react');
var ReactDOM = require('react-dom');
var PropTypes = require('prop-types');
var api = require('../utils/api');
const constants = require('../utils/constants');

function SelectPortfolio(props) {
    var Portfolios = ['Content', 'Instagram'];

    return(
        <ul className='portfolio-sources'>
            {Portfolios.map(function(portfolio){

                return (
                    <li
                        style={portfolio === props.selectedPortfolio ? {borderBottom: 'solid 2px', fontWeight: 'bolder'}: null}
                        onClick={props.onSelect.bind(null, portfolio)}
                        key={portfolio}>
                        {portfolio}
                    </li>
                )
            })}
        </ul>
    )
}

function MockPhotoGrid (props) {
    return (
        <ul className='media-list'>
            {props.results.map(function (results, index) {
                return (
                    <li key={results.deviationid} className='media-item'>
                        <ul className='ig-img-container'>
                            <li>
                                <img
                                    className='ig-img ig-media'
                                    src={results.content.src || ' '}
                                    alt={'mstrkuku picture from DeviantART'+ results.deviationid}
                                    title={results.title}
                                />

                                <span className='username'>{results.title}</span>

                            </li>
                        </ul>
                    </li>
                )
            })}
        </ul>
    )
}

function IGPhotoGrid (props) {

    // props.data.map(function (data) {
    //     this.setState(function () {
    //        return {
    //            PlayPause: function () {
    //                if (this.paused) {
    //                    this.play();
    //                    // console.log(this);
    //                    console.log(event.target);
    //                }
    //                else {
    //                    this.pause();
    //                    // console.log(this);
    //                    console.log(event.target);
    //
    //                }
    //            }
    //        }
    //     });
    //
    //
    //
    // });

    // function PlayPause() {
    //     var video = document.getElementById('video');
    //
    //     // for (ig video array length add a function 'play video')
    //
    //     if (video.paused) {
    //         video.play();
    //         console.log(event.target);
    //     }
    //     else {
    //         video.pause();
    //         console.log(event.target);
    //
    //     }
    //     console.log(ReactDOM);
    // }

    return (
        <ul className='media-list'>
            {props.data.map(function (data, index) {
                function PlayPause() {
                    var video = document.getElementById('video-'+index);
                    console.log('the data', data, index);

                    // for (ig video array length add a function 'play video')

                    if (video.paused) {
                        video.play();
                        console.log(event.target);
                    }
                    else {
                        video.pause();
                        console.log(event.target);

                    }
                    console.log(ReactDOM);
                }
                return (
                    <li key={data.id} className='media-item'>
                        <ul className='ig-img-container'>
                            <li>
                                {!data.videos
                                    ? <span>
                                        <img
                                            className='ig-img ig-media'
                                            src={data.images.standard_resolution.url}
                                            alt={'@mstrkuku picture from IG'+ data.id}
                                            title={data.title || 'Caption: ' + data.caption.text + '. This post got ' + data.likes.count + ' likes.'}/>
                                    </span>

                                    :
                                   <button className="video-btn" onClick={PlayPause.bind(this)}>
                                        <video
                                               // onClick={console.log(this)}
                                            className='ig-video ig-media' id={'video-'+index}>
                                            <source src={data.videos.standard_resolution.url || ''}/>
                                        </video>
                                    </button>

                                }
                                <span className='username'>{data.tags[0]}</span>

                            </li>
                        </ul>

                        {/*<div className="ig-media-info">*/}
                            {/*<span className="ig-likes">*/}
                                {/*/!*<span className='heart'>❤</span>*!/*/}
                                {/*{data.likes.count} likes*/}
                            {/*</span>*/}

                            {/*<br/>*/}

                            {/*<span className='ig-caption'>{data.caption.text}</span>*/}
                        {/*</div>*/}
                    </li>
                )
            })}
        </ul>
    )
}

IGPhotoGrid.propTypes = {
    data: PropTypes.array.isRequired
};

MockPhotoGrid.propTypes = {
    results: PropTypes.array.isRequired
};

SelectPortfolio.propTypes = {
    selectedPortfolio: PropTypes.string.isRequired,
    onSelect: PropTypes.func.isRequired,

};


class Gallery extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            selectedPortfolio: 'Instagram',
            data: null,
            err: null
        };
        this.updatePortfolio = this.updatePortfolio.bind(this);
    }
    componentWillMount() {
        // api.getRandomImages();
        // api.fetchTaggedPics();
        this.state.DevArt = api.getMockedDeviant();
    }

    componentDidMount () {
        //AJAX
        this.updatePortfolio(this.state.selectedPortfolio);
    }

    componentDidUpdate(prevProps, prevState) {
        if(prevState.selectedPortfolio !== this.state.selectedPortfolio) {
            this.updatePortfolio(this.state.selectedPortfolio);
        }
    }

    updatePortfolio(portfolio) {
        this.state.oneToThree = Math.floor((Math.random() * 10) + 1);

        this.setState(function() {
            return {
                selectedPortfolio: portfolio,
                data: null,
            }
        });


        // INSTAGRAM PREVIEWED PICS
        // @TODO This the preferred way to handle

        // if (this.state.selectedPortfolio === 'Instagram') {
        // api.fetchTaggedPics()
        //     .then(function (data) {
        //         console.info(this.state.selectedPortfolio);
        //         this.setState(function () {
        //             return {
        //                 // selectedPortfolio: portfolio,
        //                 data: data,
        //             }
        //         })
        //     }.bind(this))
        //
        // } else if (this.state.selectedPortfolio === 'Content') {
        //         (function () {
        //             console.log('this.state.DevArt = api.getMockedDeviant()', this.state.DevArt);
        //         // }, 3000)
        //         // .then(function (){
        //             console.info(this.state.selectedPortfolio);
        //             this.setState(function() {
        //                 return {
        //                     data: this.state.DevArt,
        //                 }
        //             });
        //
        //         }.bind(this));
        //     }

        api.fetchTaggedPics()
            .then(function (data) {

                if (this.state.selectedPortfolio === 'Instagram') {
                    console.info(this.state.selectedPortfolio);
                    this.setState(function () {
                        return{
                            selectedPortfolio: portfolio,
                            data: data,
                            err: null
                        }
                    })
                } else if (this.state.selectedPortfolio === 'Content') {
                    console.info(this.state.selectedPortfolio);
                    this.setState(function() {
                        return {
                            data: this.state.DevArt,
                            err: null
                        }
                    });
                }
            }.bind(this))

            .catch(function(err){
                 this.state.selectedPortfolio = 'Content';
                    console.info(this.state.selectedPortfolio);
                    this.setState(function() {
                        return {
                            data: this.state.DevArt,
                            err: null
                        }
                    });
                this.state.err = err;
                console.warn('Oops, Error occurred fetching IG pictures.', err.message);
                this.setState(function() {
                    return {
                        err: err
                    }
                });
                console.log();
            }.bind(this));

    }


    render() {
        return (
            <div className='gallery-container'>

                <SelectPortfolio selectedPortfolio={this.state.selectedPortfolio} onSelect={this.updatePortfolio}/>
                {this.state.data
                    ? <div>{this.state.selectedPortfolio === 'Instagram'
                        ? <IGPhotoGrid data={this.state.data}/>
                        : <MockPhotoGrid results={this.state.data}/>
                    }</div>

                    : <div className='loading'>
                        {!this.state.err
                            ?<img className='loading-img' src={'../app/assets/img/' + this.state.oneToThree + '.gif'} alt={'Loading...'}/>
                            :<p> Wow {this.state.err.message}, Might wanna get that checked out.
                                <img className='loading-img' src="../app/assets/img/404.gif" alt="error"/>
                            </p>
                            }
                    </div>
                }
            </div>

        )
    }
}

module.exports = Gallery;