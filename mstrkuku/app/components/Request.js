import React, { Component } from 'react';
var PropTypes = require('prop-types');
var api = require('../utils/api');
const constants = require('../utils/constants');
// var firebase = require("../firebase");
import * as firebase from '../firebase';


// IGPhotoGrid.propTypes = {
//     data: PropTypes.array.isRequired
// };

class Request extends Component {
    constructor (props) {
        super(props);
        this.state = {
            questions: constants.REQUEST_FORM_QUESTIONS,
            answers: {},
            submit: ''
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.nextQuestion = this.nextQuestion.bind(this);
        this.check = this.check.bind(this);
        // this.editInputs = this.editInputs.bind(this);
    }

    handleChange(event, question) {
        var value = event.target.value;

        this.setState(prevState => {
            return {
                answers: Object.assign(prevState.answers, {[question]: value})
            }
        })
    }

    handleSubmit(event) {
        event.preventDefault();

        console.log({Questions: this.state.questions, Answers: this.state.answers});
        alert("Request Sent, you will be Contacted Soon.");
        // this.props.onSubmit(
        //     this.state.answers
        // );
        const answerRef = firebase.database().ref("answer");
        const answer = {
            question: this.state.questions,
            answer: this.state.answers
        };
        answerRef.push(answer);
        this.setState({
            question: "",
            answer: ""
        });
        this.state.submit = true;
        // for (var ind = 0; ind < this.state.answers; ind++) {
        //     console.warning('Empty -->', ind);
        //
        //     if (this.state.answers[ind] === '' || this.state.answers[ind] === null || this.state.answers[ind] === undefined) {
        //         delete this.state.answers[ind];
        //     }
        // }
    }

    nextQuestion(event, question, type) {
        event.preventDefault();
        console.log(question);
        console.log('questions.type', type);
        // console(this.state);
        // let firstChildID = document.getElementById('form-question-1');
        let questionID = event.target.parentNode;
        let questionInputsParent = event.target.parentNode.childNodes;
        let nextQuestion = event.target.parentNode.nextSibling;
        // console.log('sibling', nextQuestion);
        let questionInput = event.target.parentNode.childNodes[0].getElementsByClassName('requester-textbox')[0].value;
        let questionTitle = event.target.parentNode.childNodes[0].getElementsByClassName('requester-textbox')[0].id;
        let errorMsg = questionInputsParent[0].getElementsByClassName('error')['error'];
        // var questionID = document.getElementById("form-question-1");
        // var questionID = this;
        // console.log("this index is: ", questionID);
        console.log('this question\'s Parent input is: ', questionInputsParent);
        console.log('found this "error" span: ', questionInputsParent[0].getElementsByClassName('error')['error']);
        console.log('this question\'s '+ event.target.id +' input is: ', questionInput);
        console.log('this question\'s '+ event.target.id +' TITLE is: ', questionTitle);

        for (let index = 0; index < 6; index++) {
            let actualIndex = question.replace('Question ', '');
            console.debug('question type', type);
            console.log('QWEST', questionTitle);

            // @TODO add error message handling on each form

            if (type === 'email' && questionInput.length <= 0 || (type!== 'email' && questionInput.length <= 0)) {
                setTimeout(function() {
                    errorMsg.classList.add("changeHeight");
                    errorMsg.innerHTML = 'You Gotta give me something... <br/> Just say "N/A"';
                }, 100);
                setTimeout(function() {
                    errorMsg.classList.remove("changeHeight");
                    errorMsg.innerHTML = '';
                }, 5000);

                console.log('dis is your length', questionInput.length);

            } else if (type === 'email' && !questionInput.includes('@')) {

                setTimeout(function() {
                    errorMsg.classList.add("changeHeight");
                    errorMsg.innerHTML = questionID.parentNode[0].validationMessage;
                }, 100);

                setTimeout(function() {
                    errorMsg.classList.remove("changeHeight");
                    errorMsg.innerHTML = '';
                }, 5000);
            } else {
                errorMsg.classList.remove("changeHeight");
                errorMsg.innerHTML = '';

                if (actualIndex < 1) {
                    // firstChildID = questionID;
                    // console.log('the FIRST BORN', firstChildID);
                    console.log('the #1 index', index );
                } else if (actualIndex === 6) {
                    index = 0;
                    // console.log('the end INDEX IS', firstChildID);
                    // firstChildID.classList.add("hidden-til-further");
                    // firstChildID.classList.remove("show-dis");
                } else {
                    console.log('the normal questionID IS', questionID);
                    // console.log('the firstBorn ID IS', firstChildID);
                    console.log('the actual INDEX IS', actualIndex);

                    // console.log(check());
                    sessionStorage.setItem(questionTitle + ' Answer', questionInput);
                    // questionID.classList.add("animate-in");
                    // questionID.classList.remove("animate-out");

                    questionID.classList.add("hidden-til-further");
                    nextQuestion.classList.remove("hidden-til-further");

                    // nextQuestion.classList.remove("animate-in");
                    // nextQuestion.classList.add("animate-out");
                    // nextQuestion.classList.add("animate-in");

                    questionID.classList.remove("show-dis");
                    nextQuestion.classList.add("show-dis");

                    // nextQuestion.classList.remove("animate-out");

                }
            }
        }
    }

    check(event) {
        event.preventDefault();

        if (!this.state.answers) {
            index = 0;
        }

        this.state.clicked = true;
        console.log('the CLICKED', this.state.clicked);
        console.log('the SUBMITTED', this.state.submit);
        console.log('the EVENT', event);
    }

    // editInputs(event) {
    //     event.preventDefault();
    //     // var requesterTextBoxes = document.querySelectorAll('.requester-textbox');
    //     //
    //     // // if (requesterTextBoxes)
    //     // requesterTextBoxes.classList.remove("hidden-til-further");
    //     // requesterTextBoxes.classList.add("show-dis");
    //     console.log(this.state.answers);
    //     return this.Results();
    // }

    renderList() {
        return this.state.questions.map((questions, index) => {
            return (
                <div key={index} id={'form-question-'+questions.id} className={index === 0 ? 'show-dis' : 'hidden-til-further'}>

                    {questions.type === 'textarea'
                        ?<span className="question-container">
                            <span className="chocolate">STEP {index+1} OF {this.state.questions.length}</span>
                            <h2>{questions.question}</h2>
                            <span className="error" id="error"></span>

                            <textarea className="requester-textbox"
                                      id={questions.title}
                                      rows="4"
                                      placeholder="Write here..."
                                      maxLength="500"
                                      value={this.state.questions.title}
                                      onChange={(e) => this.handleChange(e, questions.title)}
                                      required
                            />
                        </span>
                        : <span className="question-container">
                            <span className="chocolate">STEP {index+1} OF {this.state.questions.length}</span>
                            <h2>{questions.question}</h2>
                            <span className="error" id="error"></span>

                            {this.state.answers[index]
                                ? <label htmlFor={questions.title}> {questions.disclaimer} </label>
                                : null
                            }

                            <input className='requester-textbox'
                                   type={questions.type}
                                   id={questions.title}
                                   placeholder='Write here...'
                                   autoComplete={questions.type}
                                   value={this.state.questions.title}
                                   onChange={(e) => this.handleChange(e, questions.title)}
                                   required
                            />
                        </span>
                    }
                    {/* @TODO input type is Determined by the a variable a question provides for*/}
                    <br/>
                    <button id={'input-question-' +questions.id + '-btn'}
                            className="requester-confirm-btn small-btn btn"
                            onClick={(e)=>this.nextQuestion(e, questions.title, questions.type)}>NEXT>>
                    </button>
                    {/* @TODO Only Next if Less than length of array */}
                </div>
            )
        })
    }

    theResults() {
        return this.state.answers.map((answers, index) => {
            return (
                <div key={index}>
                    <h3>Question {index+1}</h3>
                    <p>{answers}</p>
                </div>
            )
        })
    }

    render() {
        return (
            <div className='request-container'>

                {/*<section>*/}
                    {/*<div className='big-btn'><img src={constants.REQUEST_ICON}/></div>*/}
                    {/*<span className='big-btn-label'>Request Work</span>*/}
                {/*</section>*/}


                {/*<section>*/}
                    {/*<div className='big-btn'><img src={constants.CHAT_ICON}/></div>*/}
                    {/*<span className='big-btn-label'>Direct Message</span>*/}


                    {/*<div className='btn-container'>*/}
                        {/*<span className='pulse-button'>!</span>*/}
                    {/*</div>*/}

                {/*</section>*/}

                <div className="requester-form-container">
                    <form className="requester-form" onSubmit={this.handleSubmit}>
                        {this.renderList()}
                        <div className={!this.state.answers ? "request-container show-dis" : "request-container hidden-til-further"}>
                            <input className="btn primary-btn" type="submit" value="REQUEST" disabled={!this.state.answers}/>
                            {/*<button className='btn secondary-btn requester-textbox' onClick={this.nextQuestion}>EDIT</button>*/}
                        </div>
                    </form>

                    <div className={!this.state.submit ? 'hidden': 'shown'}>
                        <p>Thanks I'll Get Back To You ASAP!</p>
                        {/*{this.theResults()}*/}
                    </div>

                    {/*<div>*/}
                        {/*{this.state.submit*/}
                            {/*?<span>{this.Results()}</span>*/}
                            {/*: <span><button onClick={this.editInputs}>AW YEAH!</button></span>*/}
                            {/*}*/}
                    {/*</div>*/}

                </div>

                    {/*{this.state.submit */}
                        {/*? <div>*/}
                            {/**/}
                            {/*<h4>{this.state.questions}</h4>*/}
                            {/*<b>{this.state.answers}</b>*/}
                        {/*</div> */}
                        {/*: null*/}
                    {/*}*/}

            </div>

        )
    }
}

Request.propTypes = {
    // onSubmit: PropTypes.func.isRequired,
};

module.exports = Request;