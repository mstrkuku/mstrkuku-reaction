var React = require('react');
var PropTypes = require('prop-types');
var api = require('../utils/api');
const constants = require('../utils/constants');



class Login extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            currentQuestion: '',
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        var value = event.target.value;
        
        this.setState(function () {
            return {
                currentQuestion: value
            }
        })
    }
    handleSubmit(event) {
        event.preventDefault();

        this.props.onSubmit(
            this.props.id,
            this.state.currentQuestion,
        )
    }

     render() {
         return (
             <form className='' onSubmit={this.handleSubmit}>
                 {/*<label className='' htmlFor='currentQuestion'>*/}
                 {/*</label>*/}

                 <input
                     id='currentQuestion'
                     placeholder='Question'
                     type='text'
                     autoComplete='off'
                     value={this.state.currentQuestion}
                     onChange={this.handleChange}
                 />
                 <button
                    className="button"
                    type='submit'
                    disabled={!this.state.currentQuestion}>
                     Go
                 </button>
             </form>


         )
     }
 }


 Question.propTypes = {
   onSubmit: PropTypes.func.isRequired,
 };

module.exports = Question;