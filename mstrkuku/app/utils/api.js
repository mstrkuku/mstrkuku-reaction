var axios = require('axios');
// var Cloudinary = require('cloudinary-react');
var deviantArt = require('./deviantArt.json');

const constants = require('./constants');

module.exports = {
    fetchTaggedPics: function () {
        var dm = this;
        const instaAccessID = '1062531431.a501e9b.279b9b3f520c43689f45c70fa5d857d7';
        var tagName = 'mstrkuku';
        var encodedURI = window.encodeURI('https://api.instagram.com/v1/tags/'
            +tagName+ '/media/recent?access_token='+ instaAccessID);

        return axios.get(encodedURI)
            .then(function(response) {
                console.debug(encodedURI);
                console.debug("IG response.data.results: ",response.data.results);
                console.debug('IG response.data.data: ', response.data.data);
                console.debug("IG response.data",response.data);
                console.debug("IG response.results",response.results);
                console.table('this', dm);

                return response.data.data;
            })
    },

    getDeviantAccess: function () {

        console.log('LOOK YO!!');

            var encodedURI = window.encodeURI('https://www.deviantart.com/oauth2/token?' +
                'grant_type=client_credentials&' +
                'client_id=7389&' +
                'client_secret=e342f510fab8fb0503b67fcf78252fb6');

            return axios.get(encodedURI)
                .then(function(response) {

                    var access = response.data.access_token;

                    console.log(encodedURI);
                    console.log('access from getDeviantAccess', access);
                    return access;
                });
        // }
    },

    fetchPortfolioPics: function () {
        // debugger;
        module.exports.getDeviantAccess();
        console.log('DUDE WATCH --> ', module.exports.getDeviantAccess());
        // var access = module.exports.getDeviantAccess();
        // const access = sessionStorage.getItem('DeviantArt');
        // const access = '124bd5744524386b22f87f3dc0ed9796877e762fab39125f14';
        // const access = access;
        const deviantAccessID = access;
        const username = 'mstrkuku';
        const deviantMethod = '/gallery/all';
        console.log('THE TOKEN', access);



        var encodedURI = window.encodeURI('https://www.deviantart.com/api/v1/oauth2'+
            deviantMethod+'?username='+username+
            '&mature_content=true&response_type=code&access_token=' + deviantAccessID);
        // var encodedURI = window.encodeURI('https://www.deviantart.com/api/v1/oauth2/gallery/all?username=mstrkuku&mature_content=true&response_type=code&access_token=d2e6d07255e4956e24e84d9068bd0d7da6452d74839d55ec6d');

        return axios.get(encodedURI)
            .then(function(response) {
                console.debug(encodedURI);
                console.debug("DA response.data.results: ",response.data.results);
                console.debug('DA response.data.data: ', response.data.data);
                console.debug("DA response.data",response.data);
                console.debug("DA response.results",response.results);

                return response.results;
            });
    },

    getRandomImages : function () {
        var vm = this;
        var randomImageURI = window.encodeURI('https://jsonplaceholder.typicode.com/albums/1/photos');

        return axios.get(randomImageURI).then(function(response){
            console.log('theb RANDOM URI is ', randomImageURI);
            console.log('the RANDOM RESPONSE' , response);
            console.table('this', vm);

            return response.data;

        })
    },

    getMockedDeviant : function() {
        var theArt = deviantArt.results;
        console.log('theArt', theArt);
        return theArt;
    }

        // return axios.get(encodedURI)
        //     .then(function(response){
        //         return response.data.data;
        //     })
    // }
};
