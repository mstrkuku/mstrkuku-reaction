var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var webpack = require('webpack');

let config = {
    //actual code
    entry: './app/index.js',
    //where it goes when complied and transformed
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'index_bundle.js',
        publicPath: '/'
    },

    module: {

        rules: [
            // { test: /\.(js)$/, use: 'babel-loader'},
            {test: /\.jsx?$/, exclude: /node_modules/, loader: 'babel-loader'},
            // style-loader: creates style nodes from JS strings
            // css-loader: translates CSS into CommonJS
            // sass-loader: compiles Sass to CSS
            { test: /\.(css)$/, use: ['style-loader', 'css-loader']},
            { test: /\.(sass|scss)$/, use: ['style-loader', 'css-loader', 'sass-loader']},
        ]
    },
    devServer: {
        historyApiFallback: true,
    },

    plugins: [new HtmlWebpackPlugin({
        template: 'app/index.html'
    })]

};

if (process.env.NODE_ENV === 'production') {
    config.plugins.push(
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify(process.env.NODE_ENV)
            }
        }),
        new webpack.optimize.UglifyJsPlugin()
    )
}

module.exports = config;
