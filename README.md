# README #

![Mr Kuku Icon|100x100,60%](http://www.mstrkuku.com/app/img/mysterkuku-orig.png)
<img src="http://www.mstrkuku.com/app/img/mysterkuku-orig.png" width="60px" height="60px"/>

___

mstrkuku.com : V2 Requester Site

### What is this repository for? ###
------------- 

* A Site that Displays Design Work and Gives Visitors the Ability to Request for Custom Designs
* Version 2
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###
------------- 


Summary of set up
    
    git clone
    
    cd mstrkuku/
   
    yarn 
    
    Create Script (if not made).
    
    "start" : "webpack-dev-server --open"
    
    yarn run start
  

Configuration

Dependencies
        
        react
        react-dom
        axios
        
        babel-core 
        babel-loader 
        babel-preset-env 
        babel-preset-react 
        babel-core 
        CSS Loader 
        html-webpack-plugin 
        Node SASS 
        SASS Loader 
        Style Loader 
        Webpack 
        Webpack Dev Server

Database configuration

        Firebase


* How to run tests

Deployment instructions

Create Scripts (if not made).

    "build" : "NODE_ENV='production' webpack -p "
    "deploy" : "npm run build && firebase deploy"
    "firebase-init" : firebase login && firebase init



### Contribution guidelines ###
------------- 


* Writing tests in jest or jasmine
* Code review
* Other guidelines

### Who do I talk to? ###
------------- 

* *@mstrkuku*
* one man team for now.


### Services ###
------------- 
| Front End | Compiler | Navigation | Style | Task Scripting | JavaScript Version | Backend | REST | Package Manager |
|-----------|-----------|-----------|-----------|-----------|-----------|-----------|-----------|-----------|
| [ReactJS (for now)](https://reactjs.org/) | [Babel](https://babeljs.io/) | [React Router](https://medium.com/@pshrmn/a-simple-react-router-v4-tutorial-7f23ff27adf) | [SASS](http://sass-lang.com/) | [ Webpack ](https://webpack.js.org/concepts/) | ES6 | [Firebase](https://console.firebase.google.com/u/1/) | Axios | [Yarn](https://yarnpkg.com/lang/en/docs/migrating-from-npm/) |

### Features ###
------------- 

* *Home Page*
* *Gallery / Instagram container*
* *Blog / Web Comic enabler* (_Stretch Goal_)
* *Requester Section / Quick Form Fill out*
* *Registration / Login*
* *Direct Message*
* *Store* (_Stretch Goal_)


_(webpack) command for creating '/dist'_